package th.ac.tu.siit.lab7database;

import android.os.Bundle;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends ListActivity {
	
	DBHelper dbHelper;
	SQLiteDatabase db;
	Cursor cursor; //for managing the records retrieved from the table
				   //it works like List<Map<String,String>> in the previous lab.
	SimpleCursorAdapter adapter; //Use Cursor as the data source

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		dbHelper = new DBHelper(this);
		db = dbHelper.getWritableDatabase(); //We can update the database
		cursor = getAllContacts();
		adapter = new SimpleCursorAdapter(this, R.layout.item, cursor, 
				new String[] {"ct_Subject", "ct_Amount", "ct_type", "ct_Date"},
				new int[] {R.id.tvSubject, R.id.tvAmount, R.id.ivPhoneType, R.id.tvEmail}, 0);
		setListAdapter(adapter);
		registerForContextMenu(getListView());
	}
	
	private Cursor getAllContacts() {
		//db.query executes "SELECT" statement.
		return db.query("contacts", //table name
				new String[] {"_id", "ct_Subject", "ct_Amount",
				"ct_type", "ct_Date"}, //list of columns
				null, //condition for WHERE clause e.g. "ct_name LIKE ?"
				null, //values for the conditions e.g. new String[] ( "John" )
				null, //GROUP BY
				null, //HAVING
				"ct_Subject asc"); //ORDER BY
		//SELECT _id, ct_name, ct_phone, ct_type, ct_email, FROM contacts
		//ORDER BY ct_name ASC;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		getMenuInflater().inflate(R.menu.context, menu);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		//Close everthing before letting the application ends
		cursor.close();
		db.close();
		dbHelper.close();
		finish();
	}

	@Override
	protected void onActivityResult(int requestCode, 
			int resultCode, Intent data) {
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			//Insert a new record to the table
			//ContentValues is a class of storing key-value pairs for the table.
			ContentValues v = new ContentValues();
			v.put("ct_Subject", data.getStringExtra("Subject"));
			v.put("ct_Amount", data.getStringExtra("Amount"));
			v.put("ct_Date", data.getStringExtra("Date"));
			v.put("ct_type", data.getStringExtra("type"));
			db.insert("contacts", null, v);
			//Refresh the ListView in order to display the new record
			//(1) Re-retrieve all the records
			cursor = getAllContacts();
			//(2) Update the cursor in the adapter
			adapter.changeCursor(cursor);
			//(3) Notify that the data have changed
			adapter.notifyDataSetChanged();
		}
		else if (requestCode == 8888 && resultCode == RESULT_OK) {
			//Insert a new record to the table
			//ContentValues is a class of storing key-value pairs for the table.
			ContentValues v = new ContentValues();
			long id = data.getLongExtra("id", -1);
			v.put("ct_Subject", data.getStringExtra("Subject"));
			v.put("ct_Amount", data.getStringExtra("Amount"));
			v.put("ct_Date", data.getStringExtra("Date"));
			v.put("ct_type", data.getStringExtra("type"));
			String selection = "_id = ?";
			String[] selectionArgs = { String.valueOf(id) };
			db.update("contacts", v, selection, selectionArgs);
			//Refresh the ListView in order to display the new record
			//(1) Re-retrieve all the records
			cursor = getAllContacts();
			//(2) Update the cursor in the adapter
			adapter.changeCursor(cursor);
			//(3) Notify that the data have changed
			adapter.notifyDataSetChanged();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.action_new:
			Intent i = new Intent(this, AddNewActivity.class);
			startActivityForResult(i, 9999);
			return true;
		case R.id.action_more:
			Intent a = new Intent(this, More.class);
			startActivityForResult(a, 8881);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo a = (AdapterContextMenuInfo)item.getMenuInfo();
		long id = a.id; //id of the selected item
		int position = a.position; //position of the selected item
		
		switch(item.getItemId()) {
		case R.id.action_edit:
			//Get the selected record
			Cursor c = (Cursor)adapter.getItem(position);
			//Get the value of "ct_name" column
			String Subject = c.getString(c.getColumnIndex("ct_Subject"));
			String Amount = c.getString(c.getColumnIndex("ct_Amount"));
			String Date = c.getString(c.getColumnIndex("ct_Date"));
			int itype = c.getInt(c.getColumnIndex("ct_type"));
			//Prepare an Intent
			Intent i = new Intent(this, AddNewActivity.class);
			//Attach the data to the intent
			i.putExtra("ct_Subject", Subject);
			i.putExtra("ct_Amount", Amount);
			i.putExtra("ct_Date", Date);
			i.putExtra("ct_type", itype); //int -> getIntExtra
			i.putExtra("id", id); //long -> getLongExtra
			//Start AddNEwActivity and wait for the result
			startActivityForResult(i, 8888);
			Toast t = Toast.makeText(this, "Selected ID = "+id+
					" with name = "+Subject, Toast.LENGTH_LONG);
			t.show();
			return true;
		case R.id.action_delete:
			String selection = "_id = ?";
			String[] selectionArgs = { String.valueOf(id) };
			db.delete("contacts", selection, selectionArgs);
			cursor = getAllContacts();
			//(2) Update the cursor in the adapter
			adapter.changeCursor(cursor);
			//(3) Notify that the data have changed
			adapter.notifyDataSetChanged();
			
			return true;
		}
		return super.onContextItemSelected(item);
	}
}
